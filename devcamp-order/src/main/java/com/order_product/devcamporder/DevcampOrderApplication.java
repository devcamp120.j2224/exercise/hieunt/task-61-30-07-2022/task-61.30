package com.order_product.devcamporder;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DevcampOrderApplication {

	public static void main(String[] args) {
		SpringApplication.run(DevcampOrderApplication.class, args);
	}

}

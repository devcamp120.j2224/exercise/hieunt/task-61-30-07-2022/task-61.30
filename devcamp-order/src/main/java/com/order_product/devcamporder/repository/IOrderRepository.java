package com.order_product.devcamporder.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.order_product.devcamporder.model.COrder;

public interface IOrderRepository extends JpaRepository<COrder, Long>{
    COrder findById(long id);
}
